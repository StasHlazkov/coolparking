﻿using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Services
{
    public class ParkingService : IParkingService
    {
        private readonly string url;
        private readonly HttpClient _client;
        public ParkingService(IConfiguration config)
        {
            url = config.GetValue<string>("WebServerUrl");
            _client = new HttpClient();
        }
        public async Task AddVehicle(Vehicle newVehicle)
        {
            var json = JsonConvert.SerializeObject(newVehicle);
            StringContent httpContent = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

            await _client.PostAsync(url + "vehicles" + "/", httpContent);
        }

        public async Task<Vehicle> GetVehicleByID(string id)
        {

            var vehicle = await _client.GetStringAsync(url + "vehicles" + "/" + "getbyid" + "/" + $"{id}/");
            return JsonConvert.DeserializeObject<Vehicle>(vehicle);
        }

        public async Task<decimal> GetBalance()
        {
            var vehicleBalance = await _client.GetStringAsync(url + "parking" + "/" + "balance/");
            return JsonConvert.DeserializeObject<decimal>(vehicleBalance);
        }

        public async Task<int> GetCapacity()
        {
            var vehicle = await _client.GetStringAsync(url + "parking" + "/" + "capacity/");
            return JsonConvert.DeserializeObject<int>(vehicle);
        }

        public async Task<int> GetFreePlaces()
        {
            var vehicle = await _client.GetStringAsync(url +  "parking" + "/" + "freePlaces/");
            return JsonConvert.DeserializeObject<int>(vehicle);
        }

        public async Task<TransactionInfo[]> GetLastParkingTransactions()
        {
            var lastTransaction = await _client.GetStringAsync(url + "transactions" + "/" + "last/");
            return JsonConvert.DeserializeObject<TransactionInfo[]>(lastTransaction);
        }
        public async Task<bool> CheckIfExistTransactionByVehicleId(string Id)
        {
            var vehicleExist = await _client.GetStringAsync(url + "/" + "transactions" + "/" + "checktransaction/" +"/" + $"{Id}/");
            return JsonConvert.DeserializeObject<bool>(vehicleExist);
        }

        public async Task<IReadOnlyCollection<Vehicle>> GetVehicles()
        {

            var vehicles = await _client.GetStringAsync(url +"vehicles" + "/");
            return JsonConvert.DeserializeObject<IReadOnlyCollection<Vehicle>>(vehicles);
        }

        public async Task<string> ReadFromLog()
        {
            return await _client.GetStringAsync(url + "transactions" + "/" + "all/");            
        }

        public async Task RemoveVehicle(string vehicleId)
        {

            await _client.DeleteAsync(url + "vehicles" + "/" + "deletebyid" + "/" + $"{vehicleId}/");

        }

        public async Task TopUpVehicle(string vehicleId, decimal sum)
        {
            string json = JsonConvert.SerializeObject(vehicleId +"/"+ sum);
            StringContent httpContent = new StringContent(json, System.Text.Encoding.UTF8, "application/json");
            await _client.PutAsync(url +"transactions" + "/" + "topUpVehicle/", httpContent);       
        }   
    }
}