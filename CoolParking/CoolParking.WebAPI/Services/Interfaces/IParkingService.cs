﻿using CoolParking.WebAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Interfaces
{
    public interface IParkingService
    {
        Task<decimal> GetBalance();
        Task<int> GetCapacity();
        Task<int> GetFreePlaces();
        Task<IReadOnlyCollection<Vehicle>> GetVehicles();
        Task AddVehicle(Vehicle vehicle);
        Task RemoveVehicle(string vehicleId);
        Task TopUpVehicle(string vehicleId, decimal sum);
        Task<TransactionInfo[]> GetLastParkingTransactions();
        Task<string> ReadFromLog();
        Task<bool> CheckIfExistTransactionByVehicleId(string Id);
        Task<Vehicle> GetVehicleByID(string id);

    }
}
