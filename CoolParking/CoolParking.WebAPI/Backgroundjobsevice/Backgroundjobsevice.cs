﻿using CoolParking.BL.CMD;
using CoolParking.BL.Services;
using Microsoft.Extensions.Hosting;
using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Backgroundjobsevice
{
    public class BackGroundJobService : IHostedService, IDisposable
    {
        public BackGroundJobService()
        {

        }
        private Thread pollThread;
        private CancellationTokenSource cancellationTokenSource;
        public Task StartAsync(CancellationToken cancellationToken)
        {
            this.cancellationTokenSource = new CancellationTokenSource();
            
            this.pollThread = new Thread(Start);
            this.pollThread.Start();
            return Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            this.cancellationTokenSource.Cancel();

            await Task.Run(() => { this.pollThread.Join(); }, cancellationToken);
        }

        private void Start()
        {
            TimerService timerServiceForWithdraw = new TimerService();
            TimerService timerServiceForlog = new TimerService();
            LogService logService = new LogService();
            ParkingService parkingService = new ParkingService(timerServiceForWithdraw, timerServiceForlog, logService);
            ApplicationStart._parkingService = parkingService;

            HttpListener httpListener = new HttpListener();
            while (true)
            {
                try
                {
                    ApplicationStart.Listen(httpListener);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("something went wrong" + ex.Message);
                    ApplicationStart.Stop(httpListener);
                }
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
