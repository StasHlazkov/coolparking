﻿using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Models
{
    public class Vehicle
    {
        private const string pattern = @"^\w{2}-[0-9]{4}-\w{2}";

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("vehicletype")]
        public int VehicleType { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        public static bool ValidId(string id)
        {
            if (Regex.IsMatch(id, pattern))
            {
                return true;
            }
            return false;
        }
    }
}
