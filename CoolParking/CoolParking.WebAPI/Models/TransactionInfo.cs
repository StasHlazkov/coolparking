﻿using Newtonsoft.Json;
using System;

namespace CoolParking.WebAPI.Models
{
    public class TransactionInfo
    {
        [JsonProperty("transactiondate")]
        public DateTime TransactionDate { get; private set; }

        //[Required]
        //[RegularExpression(@"^\w{2}-[0-9]{4}-\w{2}*", ErrorMessage = "ID must have format ХХ-YYYY-XX(where Х - leter, Y-number)")]
        [JsonProperty("vehicleid")]
        public string VehicleId { get; private set; }

        [JsonProperty("sum")]
        public decimal Sum { get; private set; }      
    }
}
