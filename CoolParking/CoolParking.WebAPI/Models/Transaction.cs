﻿using Newtonsoft.Json;
using System;

namespace CoolParking.WebAPI.Models
{
    public class Transaction
    {
        [JsonProperty("vehicleid")]
        public string VehicleId { get; set; }
        
        [JsonProperty("sum")]
        public decimal Sum { get; set; }
        
        [JsonProperty("transactiondate")]
        public DateTime transactionDate { get; set; }
    }
}
