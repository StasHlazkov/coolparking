﻿using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/vehicles")]
    public class VehiclesController : ControllerBase
    {
        #region Private fields
        private readonly IParkingService _parkingService;
        #endregion

        #region Constructors
        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        #endregion

        #region Web methods
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            try
            {
                var vehicles = await _parkingService.GetVehicles();
                return Ok(vehicles);
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<ActionResult> Index(Vehicle newVehicle)
        {
            try
            {
                var vehicle = await _parkingService.GetVehicleByID(newVehicle.Id);                
                if (vehicle != null)
                {
                    return BadRequest();
                }
                await _parkingService.AddVehicle(newVehicle);

                var vehicleCreated = await _parkingService.GetVehicleByID(newVehicle.Id);
                if (vehicleCreated != null)
                {
                    return Ok(vehicle);                    
                }
                return BadRequest();

            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Vehicle>> GetVehiclesById(string id)
        {
            return Ok(await _parkingService.GetVehicleByID(id));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteVehicles(string id)
        {
            try
            {
                if (!Vehicle.ValidId(id))
                {
                    return BadRequest();
                }
                await _parkingService.RemoveVehicle(id);
                var vehicle = await _parkingService.GetVehicleByID(id);
                if(vehicle == null)
                {
                    return StatusCode(204);
                }
                else
                {
                    return BadRequest("Delete was invalid");
                }
            }
            catch(ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
        }
        #endregion
    }
}
