﻿using CoolParking.WebAPI.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/parking")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        #region Private fields
        private readonly IParkingService _parkingService;
        #endregion

        #region Constructors
        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        #endregion

        #region Web methods
        [HttpGet("balance")]
        public async Task<decimal> GetParkingBalance()
        {            
            return await _parkingService.GetBalance();
        }
        [HttpGet("capacity")]
        public async Task<int> GetCapacity()
        {
            return await _parkingService.GetCapacity();
        }

        [HttpGet("freePlaces")]
        public async Task<int> GetFreePlaces()
        {
            return await _parkingService.GetFreePlaces();
        }
        #endregion
    }
}
