﻿using CoolParking.WebAPI.Interfaces;
using CoolParking.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/transactions")]
    public class TransactionsController : ControllerBase
    {
        #region Private fields
        private readonly IParkingService _parkingService;
        #endregion

        #region Constructors
        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }
        #endregion

        #region Web methods
        [HttpGet("last")]
        public async Task<ActionResult<TransactionInfo[]>> GetLast()
        {
            try
            {
                var transactions = await _parkingService.GetLastParkingTransactions();
                if (transactions != null)
                {
                    return Ok(transactions);
                }
                return BadRequest("not found any transaction");
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("all")]
        public async Task<ActionResult<string>> GetAll()
        {            
            return Ok(await _parkingService.ReadFromLog());
        }

        [HttpPut("topUpVehicle")]
        public async Task<ActionResult<Vehicle>> UpdateVehicle(string id, decimal sum)
        {
            try
            {
                await _parkingService.TopUpVehicle(id, sum);
                var vehicle = await _parkingService.GetVehicleByID(id);
                if (!await _parkingService.CheckIfExistTransactionByVehicleId(id))
                {
                    return NotFound("Trasaction wasn't added!");
                }
                else if(vehicle == null)
                {
                    return NotFound("Vehicle is absent");
                }
                else
                {
                    return Ok(vehicle);
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
        #endregion
    }
}
