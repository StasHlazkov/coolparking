﻿using System;
using System.Net;
using System.Threading.Tasks;

namespace CoolParking.BL.CMD.HttpActionBase
{
    public abstract class BaseHttpAction
    {
        private bool _post;
        private readonly HttpListener _listener;
        private readonly string _url;
        private readonly string _controler;
        private readonly string _parameter;

        public BaseHttpAction(HttpListener listener,string url, string controler, string parameter , bool Post = false)
        {
            _post = Post;
            _url = url;
            _controler = controler;
            _parameter = parameter;
            _listener = listener;
        }

        public void StartWork()
        {
            Start(InitializateListener, Imp);
        }

        public async Task<HttpListenerContext> InitializateListener()
        {
            _listener.Prefixes.Add(_url+ _controler + _parameter);
            _listener.Start();
            Console.WriteLine("Ожидание подключений...");

            HttpListenerContext context = await _listener.GetContextAsync();
            return context;
        }
        protected void Imp(Task<HttpListenerContext> httpListener)
        {
            HttpListenerRequest request = httpListener.Result.Request;
            HttpListenerResponse response = httpListener.Result.Response;

            var result = Get(request);

            if (_post)
            {
                Post(request);
            }
            else
            {
                Get(request);
            }

        }
        protected abstract string Get(HttpListenerRequest request);

        protected abstract string Post(HttpListenerRequest request);

        protected void Start(Func<Task<HttpListenerContext>> getHttpListener,
            Action<Task<HttpListenerContext>> impHttpListener)
        {
            var httplistener = getHttpListener();
            impHttpListener(httplistener);

        }

    }
}
