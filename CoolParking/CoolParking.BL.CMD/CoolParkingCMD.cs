﻿using CoolParking.BL.Services;
using System;
using System.Net;
using System.Threading.Tasks;

namespace CoolParking.BL.CMD
{
    public class CoolParkingCMD
    {
        public static async Task Main(string[] args)
        {
            TimerService timerServiceForWithdraw = new TimerService();
            TimerService timerServiceForlog = new TimerService();
            LogService logService = new LogService();
            ParkingService parkingService = new ParkingService(timerServiceForWithdraw, timerServiceForlog, logService);
            ApplicationStart._parkingService = parkingService;

            HttpListener httpListener =  new HttpListener();
            while (true)
            {
                try
                {
                   await ApplicationStart.Listen(httpListener);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("something went wrong" + ex.Message);
                    ApplicationStart.Stop(httpListener);
                }
            }
        }

        public async  void  Start()
        {
            TimerService timerServiceForWithdraw = new TimerService();
            TimerService timerServiceForlog = new TimerService();
            LogService logService = new LogService();
            ParkingService parkingService = new ParkingService(timerServiceForWithdraw, timerServiceForlog, logService);
            ApplicationStart._parkingService = parkingService;

            HttpListener httpListener = new HttpListener();
            while (true)
            {
                try
                {
                    await ApplicationStart.Listen(httpListener);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("something went wrong" + ex.Message);
                    ApplicationStart.Stop(httpListener);
                }
            }
        }
    }
}
