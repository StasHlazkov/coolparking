﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.BL.CMD
{
    public static class ApplicationStart
    {
        #region private fields
        private const string parking = "parking";
        private const string vehicles = "vehicles";
        private const string transactions = "transactions";
        private const string url = "http://localhost:8888";
        #endregion
        public static HttpListener parkinglistenerbalance;
        public static  IParkingService _parkingService;
        

        public static async Task Listen(HttpListener parkinglistenerbalance)
        {            
            //parking
            parkinglistenerbalance.Prefixes.Add(url +"/"+ parking + "/" + "balance/");
            parkinglistenerbalance.Prefixes.Add(url + "/" + parking + "/" + "capacity/");
            parkinglistenerbalance.Prefixes.Add(url + "/" + parking + "/" + "freePlaces/");

            //vehicles
            parkinglistenerbalance.Prefixes.Add(url + "/" + vehicles + "/" + "id/");
            parkinglistenerbalance.Prefixes.Add(url + "/" + vehicles + "/");
            
            //transactions
            parkinglistenerbalance.Prefixes.Add(url + "/" + transactions + "/" + "last/");
            parkinglistenerbalance.Prefixes.Add(url + "/" + transactions + "/" + "all/");
            parkinglistenerbalance.Prefixes.Add(url + "/" + transactions + "/" + "topUpVehicle/");
            parkinglistenerbalance.Prefixes.Add(url + "/" + transactions + "/" + "checktransaction/");


            parkinglistenerbalance.Start();


            Console.WriteLine("waiting");

            while (true)
            {
                await ParkinglistenerbalanceMethod(parkinglistenerbalance);
            }
           
        }
        public static void Stop(HttpListener parkinglistenerbalance)
        {
            parkinglistenerbalance.Stop();
        }
        #region Methods
        private static async Task ParkinglistenerbalanceMethod(HttpListener httpListener)
        {
            HttpListenerContext context = await httpListener.GetContextAsync();
            HttpListenerRequest request = context.Request;
            var method = context.Request.RawUrl;
            String[] queryStringArray = method.Split('/');
            string responseString = "";
            switch (method)
            {
                //parking
                case "/" + parking + "/" + "balance/":
                    responseString = _parkingService.GetBalance().ToString();
                    break;
                case "/" + parking + "/" + "capacity/":
                    responseString = _parkingService.GetCapacity().ToString();
                    break;
                case "/" + parking + "/" + "freePlaces/":
                    responseString = _parkingService.GetFreePlaces().ToString();
                    break;


                //vehicles
                case "/" + vehicles + "/" + "id/":
                    responseString = _parkingService.GetBalance().ToString();
                    break;
                case "/" + vehicles + "/":
                    if(context.Request.HttpMethod == "POST")
                    {
                        var data = context.Request;
                        string text;
                        using (var reader = new StreamReader(data.InputStream, data.ContentEncoding))
                        {
                            text = reader.ReadToEnd();
                        }                        
                        var vehicle = JsonConvert.DeserializeObject<Vehicle>(text);
                        _parkingService.AddVehicle(vehicle);
                    }
                    else if (context.Request.HttpMethod == "DELETE")
                    {
                        responseString = _parkingService.GetBalance().ToString();
                    }
                    else if (context.Request.HttpMethod == "GET")
                    {                      
                        var venhicles = _parkingService.GetVehicles();
                        responseString = JsonConvert.SerializeObject(venhicles, Formatting.Indented);
                    }
                    break;

                //transactions
                case "/" + transactions + "/" + "last/":
                    var parkingTransactions = _parkingService.GetLastParkingTransactions();
                    responseString = JsonConvert.SerializeObject(parkingTransactions, Formatting.Indented);
                    break;
                case "/" + transactions + "/" + "all/":
                    responseString = _parkingService.ReadFromLog();
                    break;
                case "/" + transactions + "/" + "topUpVehicle/":
                    if (context.Request.HttpMethod == "PUT")
                    {
                        var data1 = context.Request;
                        string text1;
                        using (var reader = new StreamReader(data1.InputStream, data1.ContentEncoding))
                        {
                            text1 = reader.ReadToEnd();
                        }
                        var topUpVehicleParameters = JsonConvert.DeserializeObject(text1).ToString();
                        String[] parameters = topUpVehicleParameters.Split('/');

                        _parkingService.TopUpVehicle(parameters[0].ToString(), Decimal.Parse(parameters[1]));
                    }
                    break;               

                default:
                    Console.WriteLine("undefined router");
                    break;
            }

            if (queryStringArray[2] == "getbyid")
            {
                var vehicle = _parkingService.GetVehicleByID(queryStringArray[3]);
                responseString = JsonConvert.SerializeObject(vehicle, Formatting.Indented);
            }
            else if (queryStringArray[2] == "deletebyid")
            {
                _parkingService.RemoveVehicle(queryStringArray[3]);
            }
            else if (queryStringArray[2] == "transactions" && queryStringArray[3] == "checktransaction")
            {

                var exist = _parkingService.CheckIfExistTransactionByVehicleId(queryStringArray[5].ToString());
                if (exist)
                {
                    responseString = JsonConvert.SerializeObject(exist, Formatting.Indented);
                }
                else 
                {
                    responseString = JsonConvert.SerializeObject(exist, Formatting.Indented);
                }
            }
            
            HttpListenerResponse response = context.Response;
            byte[] buffer = Encoding.UTF8.GetBytes(responseString);
            response.ContentLength64 = buffer.Length;
            Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            output.Close();         

        }

        #endregion      
    }
}
