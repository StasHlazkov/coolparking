﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        private const string pattern = @"^\w{2}-[0-9]{4}-\w{2}";
        private decimal _balance;      
        private string _id;
        private readonly VehicleType _vehicleType;
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = id;
            _vehicleType = vehicleType;
            Balance = balance;
        }
        public string Id
        {
            get
            {
                return _id;
            }
            private set
            {
                if(ValidId(value))
                {
                    _id = value;
                }
                else
                {
                    throw new ArgumentException("Invalid Id format");
                }
            }
        }
        public decimal Balance
        {
            get
            {
                return _balance;
            }
            private set
            {
                if(value >= 0)
                {
                    _balance = value;
                }
                else
                {
                    throw new ArgumentException("Balance can't be negative number"); 
                }
            }
        }
        public VehicleType VehicleType => _vehicleType;

        public bool AddToBalance(decimal sum)
        {
            if(sum > 0)
            {
                _balance += sum;
                return true;
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public bool WithdrawFromBalance(decimal sum)
        {
            if (sum > 0)
            {
                _balance -= sum;
                return true;
            }
            else
            {
                throw new ArgumentException("Sum can't be negativ");
            }
        }

        public static bool ValidId(string id)
        {
            if(Regex.IsMatch(id, pattern))
            {
                return true;
            }
            return false;
        }

        public static bool ValidType(VehicleType vehicleType)
        {
            if(vehicleType == VehicleType.PassengerCar || 
                vehicleType == VehicleType.Truck ||
                vehicleType == VehicleType.Bus ||
                vehicleType == VehicleType.Motorcycle)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public string GenerateRandomRegistrationPlateNumber()
        {
            return "";
        }        
    }
}