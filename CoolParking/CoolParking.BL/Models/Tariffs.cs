﻿namespace CoolParking.BL.Models
{
    public class Tariffs
    {
        public decimal PassengerCar;
        public decimal Truck;
        public decimal Bus;
        public decimal Motorcycle;
        public Tariffs(decimal PassengerCar, decimal Truck, decimal Bus, decimal Motorcycle)
        {
            this.PassengerCar = PassengerCar;
            this.Truck = Truck;
            this.Bus = Bus;
            this.Motorcycle = Motorcycle;
        }
    }
}
